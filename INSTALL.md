# Installing Julia


The best way to install julia is to grab the binaries from [julialang](https://julialang.org/downloads/). These notebooks are tested using Julia 1.1, but should work with all verions of julia 1.x. 



## Installing Jupyter notebooks


You can install Jupyter the normal way: see instructions [here](https://jupyter.org/install). To get Jupyter to recognize Julia you need to install [IJulia](https://github.com/JuliaLang/IJulia.jl).

This can be done after you install Julia. Start a Julia REPL with the `julia` command then the following

```Julia
julia> ]add IJulia
```

This will install the Julia Kernels in your base environment. The `]` key opens the package manager (all built into julia's repl), which lets you perform many package managing activities. We will go into some detail in the later notebooks, but a list of commands can be accesed through entering `?`



## Initializing the current environment and starting jupyter

To take advantage of these notebooks, we first need to instantiate the working environment with the packages used. After starting a Julia REPL in the julia-intro directory and opening the package manager with `]` you should perform the following

```Julia
(v1.1) pkg> activate .
(julia-intro) pkg> instantiate
```

This will install all the packages specified by the environment (in the Manifest.toml and Project.toml files). After this is complete, you can exit the repl using `ctrl-d` or running the function `exit()`. 

Next we want to start jupyter. In the julia-intro directory start a jupyter notebook (or a lab session) and select the notebooks/intro-1.ipynb notebook.

